function form(firstName, lastName, email, mobile){
	console.log(`First Name: ${firstName}`);
	console.log(`Last Name: ${lastName}`);
	console.log(`Email: ${email}`);
	console.log(`Mobile Number: ${mobile}`);
}

form("Juan", "Dela Cruz", "jdelacruz@gmail.com", "09261234567");
form("Taylor", "Swift", "tswift@gmail.com", "09279008010");
form("Joe", "Alwyn", "jalwynz@gmail.com", "09276502821");

